## Creación de imagen Docker y envío de la misma al repositorio de imágenes:

* Crear la imagen de Docker 
    - Situado en la raíz del proyecto, ejecutar el comando 

        ```
        docker build -t protocolo .
        ```  

* Hacer login en el repositorio de imágenes

    ```
    docker login javierperezbatista/remote-installation-protocol-with-docker
    usuario: javierperezbatista
    contraseña: 
    ```

* Hacer un tag de la imagen creada

    ```
    docker tag protocolo javierperezbatista/remote-installation-protocol-with-docker:1.0.0
    ```  

*  Hacer un push de la imagen al Docker Registry

    ```
    docker push javierperezbatista/remote-installation-protocol-with-docker:1.0.0
    ```  

## Crear proyecto laravel con Docker
* Instalar docker Desktop
* Instalar una distribución Unix (Ubuntu 18.04 LTS)
* Acceder a la linea de comandos de la distribución Unix
* Configurar la integración WLS2
```
    https://docs.docker.com/docker-for-windows/wsl/
```
* Quitar compresión y encriptado
```
    https://www.thewindowsclub.com/wslregisterdistribution-failed-with-error-0xc03a001a-2
```
* Crea proyecto Laravel
```
    curl -s https://laravel.build/example-app | bash
``` 
* Quitar antovirus

* Lanza Sail para la creación del contenedor Docker
```
    cd example-app

    ./vendor/bin/sail up
```
* Hay que mantener el Ubuntu ejecutando el comando
* Ahora el contenedor está creado y con la extension Remote WSL de VS se puede acceder al código del proyecto o a cualquier carpeta del subsistema
