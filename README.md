# Remote installation protocol with Docker

## Requisitos
* Sistema operativo Windows/Linux/MacOS
* Conexión a internet

## Instalación de Docker Desktop
Acceder a `https://www.docker.com/products/docker-desktop` y realizar la descarga para el sistema operativo oportuno. Tras la descarga, ejecutar el fichero (aceptar los permisos de seguridad de Windows si aplica) y reiniciar el equipo cuando la instalación se termine.

## Adicional
Si tras el reinicio, Docker se inicia y requiere del paquete de actualización del kernel de Linux, realizar el paso 4 y 5 de la siguiente guía de Microsoft: 
https://docs.microsoft.com/es-es/windows/wsl/install-win10#step-4---download-the-linux-kernel-update-package


## Traer imagen del repositorio
Abrir un terminal de línea de comandos (CMD o Powershell) y escribir la siguiente línea (comprobar que la aplicación Docker Dekstop se está ejecutando): 

```
    docker pull javierperezbatista/remote-installation-protocol-with-docker
```

Ahora la imagen de la aplicación de la empresa se encuentra disponible en el listado de imágenes en la aplicación Docker Desktop. 

## Acceder a la plataforma
Por último, para lanzar la imagen y crear el contenedor Docker que pondrá a disposición del usuario el entorno configurado, se debe pulsar en el botón RUN. Finalmente, el contenedor está ejecutándose y se puede acceder a él a través de un navegador web o pulsando en el botón de acceso a la plataforma que proporciona Docker Desktop 
