# Imagen base de ka que partimos.
FROM php:7.4-apache

# Actualizamos la imagen
#RUN apt-get clean all
#RUN apt-get update
#RUN apt-get dist-upgrade -y
# and optionally

# Actualizamos la imagen


# Instalamos los paquetes necesarios en la imagen
RUN apt-get update && apt-get install -y \
    git \
    zip \
    libzip-dev \
    curl \
    sudo \
    unzip \
    libicu-dev \
    libonig-dev \
    libbz2-dev \
    libpng-dev \
    libjpeg-dev \
    libmcrypt-dev \
    libreadline-dev \
    libfreetype6-dev \
    g++ \
    nano \
    libwebp-dev \
    libjpeg62-turbo-dev \
    libxpm-dev

RUN apt-get autoremove -y

# Se crean las variables de entorno para el direcotrio raiz de apache y el usuario (para darle permisos)
ENV APACHE_DOCUMENT_ROOT=/var/www/html \
    UID=1000

# Se configura apache
RUN sed -i 's/80/8081/' /etc/apache2/ports.conf
RUN sed -i 's/443/8448/' /etc/apache2/ports.conf
RUN sed -i 's/*:80/*:8081/' /etc/apache2/sites-available/000-default.conf
RUN sed -i 's/*:443/*:8448/' /etc/apache2/sites-available/default-ssl.conf

RUN sed -ri -e 's!/var/www/html!${APACHE_DOCUMENT_ROOT}!g' /etc/apache2/sites-available/*.conf
RUN sed -ri -e 's!/var/www/!${APACHE_DOCUMENT_ROOT}!g' /etc/apache2/apache2.conf /etc/apache2/conf-available/*.conf

# Se activa el módulo de reescritura de apache, el headers y el ssl
RUN a2enmod rewrite headers ssl

# Se configura php
RUN mv "$PHP_INI_DIR/php.ini-development" "$PHP_INI_DIR/php.ini"

# Se configura la instalación de la extensión gd de php
RUN docker-php-ext-configure gd \
    --with-webp \
    --with-jpeg \
    --enable-gd\
    --with-xpm \
    --with-freetype 

# Se instalan las dependencias de php
RUN docker-php-ext-install \
    bz2 \
    intl \
    bcmath \
    opcache \
    calendar \
    pdo_mysql \
    zip \
    gd \
    exif

# Se instala composer
COPY --from=composer:latest /usr/bin/composer /usr/bin/composer

# Se añade el usuario al grupo root y al grupo de apache
RUN useradd -G www-data,root -u ${UID} -d /home/devuser devuser
RUN mkdir -p /home/devuser/.composer && \
    chown -R devuser:devuser /home/devuser

RUN composer global require laravel/installer \
    laravel new hidraulicas
# Copiamos el código del proyecto a la carpeta del host de apache
COPY ./hidraulicas /var/www/html/hidraulicas

# Exponemos el puerto 8081 y el 8448
EXPOSE 8081/tcp 443/tcp 8448/tcp
